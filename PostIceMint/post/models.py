from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Post(models.Model):
	post_title = models.CharField(max_length=30)
	post_content = models.CharField(max_length=200)
	post_pub_date = models.DateField(auto_now=True)
	post_cor_date = models.DateField(auto_now=True)
	post_flag = models.BooleanField(default=True)
	post_author = models.ForeignKey(User, default=User.username)
	

	def __unicode__(self):
		return '{0} {1}'.format(self.post_title, self.post_content)