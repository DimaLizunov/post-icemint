"""PostIceMint URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
	https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
	1. Add an import:  from my_app import views
	2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
	1. Add an import:  from other_app.views import Home
	2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
	1. Import the include() function: from django.conf.urls import url, include
	2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from post import views
from post.views import post_list_view

urlpatterns = [
	url(r'^admin/', admin.site.urls),
	url(r'^blog/users/$', views.UserPost.as_view(), name ='blog_users'),
	url(r'^blog/users/(?P<id>\d+)/$', post_list_view, name='post_list'),
	url(r'^login/$',  views.Login.as_view(), name = 'login'),
	url(r'^logout/$',  views.LogOut.as_view(), name = 'logout'),
	url(r'^blog/(?P<pk>\d+)/$',  views.GetPostIDView.as_view(), name='post_id'),
	url(r'^blog/(?P<pk>\d+)/update/$',  views.PostUpdate.as_view(), name='post_update'),
	url(r'^blog/(?P<pk>\d+)/update/delete$',  views.PostDelete.as_view(), name='post_delete'),
	url(r'^blog/create/$', views.NewPost.as_view(), name = 'new_post'),
	]