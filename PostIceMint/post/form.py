from django import forms
from models import Post


class PostForm(forms.ModelForm):
	class Meta:
		model = Post
		fields = ('post_title', 'post_content', 'post_flag', 'post_author',)
		#fields = '__all__'
		#exclude = ('post_author',)
