from django.shortcuts import render, render_to_response
from post.models import Post
from post.form import PostForm
from django.views.generic import TemplateView, ListView, FormView, View, DetailView, DeleteView
from django.views.generic.edit import UpdateView
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.utils.functional import lazy
from django.core.urlresolvers import reverse_lazy
# Create your views here.


# view all users
class UserPost(ListView):
	template_name = 'post/index.html'
	model = User

	def get(self, request, *args,**kwargs):
		user = request.user
		user_list = User.objects.all()
		return render(request, self.template_name, {'user_list': user_list})

# view posts
def post_list_view(request, id):
	u = User.objects.select_related().get(id=id)
	post_list = u.post_set.all()
	return render(request, 'post/postlist.html', {'post_list': post_list})


class Login(FormView):
	form_class = AuthenticationForm
	success_url = reverse_lazy('blog_users')
	template_name = 'post/login.html'

	def form_valid(self, form):
		user = form.get_user()
		login(self.request, user)
		return super(Login, self).form_valid(form)


class LogOut(View):
    template_name = 'post/index.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login/')


# view posts by ID
class GetPostIDView(DetailView):
	model = Post
	template_name = 'post/post_id_list.html'
	# template_name_suffix = '_update_form'
	fields = ['post_title', 'post_content']


# update post
class PostUpdate(UpdateView):
	model = Post
	template_name = 'post/post_update.html'
	# template_name_suffix = '_update_form'
	success_url = reverse_lazy('blog_users')
	fields = ['post_title', 'post_content', 'post_flag']


# create posts
class NewPost(FormView):
    form_class = PostForm
    #success_url = '../../blog/users' 
    success_url = reverse_lazy('blog_users')
    template_name = 'post/new_post.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        self.object = form.save()
        return super(NewPost, self).form_valid(form)


#delete post
class PostDelete(DeleteView):
    model = Post
    template_name = 'post/del_post.html'
    success_url = reverse_lazy('blog_users')